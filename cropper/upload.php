<?php

$host=getenv('HOSTTILLIADMIN');
$hostweb=getenv('HOSTTILLIWEB');
$url = $host."clienteFotos/insert/subido/".$_GET['cliente'];
$name = str_replace( " ", "-", $_FILES['file']['name'] );

$imagenes = []; 

$imagenes['rutaImagen'] = $hostweb . 'cropper/uploads/'.$_GET['cliente']. '-' .$name;
$imagenes['cliente_id'] = $_GET['cliente'];
$imagenes['proveedor'] = 'subido';
$imagenes['impreso'] = 0;

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($imagenes));
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch);
$result = json_decode($result, false);
$result = json_decode(json_encode($result), true);
curl_close($ch);

$moved =  move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/' . $_GET['cliente']. '-' .$name);


if( $moved ) {
    echo "Successfully uploaded";         
  } else {
    echo "Not uploaded because of error #".$_FILES["file"]["error"];
  }
