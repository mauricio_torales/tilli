<?php
session_start();
$servicio=$_SESSION["url_servicio"];
?>
<!doctype html>
<html lang="es">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- CSS -->
    <link rel="stylesheet" href="../css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <script src="https://kit.fontawesome.com/46ffcb8aaa.js"></script>
    <link rel="icon" href="../favicon.ico" type="image/x-icon" />

    <title>Tilli</title>
</head>

<body>
    <nav class="navbar fixed-top navbar-expand-lg navbar-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="../index.php">
            <img class="logo" src="../img/logo.webp" alt="logo">
        </a>

        <div class="collapse navbar-collapse" id="menu">
            <ul id="nav" class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="../cuenta">
                        <img src="../img/cuenta.svg" alt="fotos">
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <section class="frecuentes">
        <div class="container">
            <center>
                <h1>PREGUNTAS FRECUENTES</h1>
            </center>
            <div class="accordion" id="accordionExample">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <button class="btn-frecuente collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                ¿Cómo funciona Tilli?
                            </button>
                        </h2>
                    </div>

                    <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                            Ofrecemos un servicio mensual / suscripción de fotografias impresas. Tenemos tres planes para ti, todos sin plazo de fidelidad o tasa de cancelación. En Tilli podes recibir fotos de tu Instagram, Facebook y también subir fotos de tu PC o celular. Adentro del ambiente de suscriptor es posible elegir cual fotos queres que lleguen a tu casa, también podes incluir un pie de foto, emojis y cantidad de likes, también fecha si fue algún acontecimiento especial que deseas recordar. Una vez al mes, se realiza una selección aleatoria desde tus redes sociales en caso de que no hayas ingresado y seleccionado las fotos que prefieres recibir, se las imprimimos en papel de alta calidad fotográfico y te la enviamos a su casa con un paquete super especial.

                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h2 class="mb-0">
                            <button class="btn-frecuente collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                ¿Como son los planes y cobranza?
                            </button>
                        </h2>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            Nuestros planes son mensuales y las cobranzas son realizadas automáticamente todos los meses mientras su plan esté activo. Hoy tenemos tres opciones de planes: Little pack con 6 fotos mensuales, Smart pack con 9 y Huge pack con 12. En todos los casos, recibirás una cajita super cute con tus fotos todos los meses. Siempre teniendo en cuenta que puedes cancelar tu plan en cualquier momento, pues no trabajamos con fidelidad o tasa de cancelación.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h2 class="mb-0">
                            <button class="btn-frecuente collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                ¿Puedo cancelar cuando quiera?
                            </button>
                        </h2>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body">
                            Sí, ¡claro que si! Aunque ofrezcamos servicio mensual, nuestros planes no tienen fidelidad o tasa de cancelación. Entonces, puedes elegir tu plan, recibir tus fotos y cancelar tu suscripción cuando quieras con un click en su cuenta o enviando un e-mail a hola@tilli.com.py solicitando la cancelación.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="heading4">
                        <h2 class="mb-0">
                            <button class="btn-frecuente collapsed" type="button" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                ¿Existe algún valor de flete?
                            </button>
                        </h2>
                    </div>
                    <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordionExample">
                        <div class="card-body">
                            Sí, existe. Todos nuestros deliverys son registrados y trackeados para mayor seguridad, por eso, existe un costo adicional que depende de tu dirección y del plan que hayas elegido. En nuestro sitio web, logras verificar el valor antes de realizar la confirmación de tu suscripción.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="heading5">
                        <h2 class="mb-0">
                            <button class="btn-frecuente collapsed" type="button" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                ¿Cuáles son las formas de pago?
                            </button>
                        </h2>
                    </div>
                    <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordionExample">
                        <div class="card-body">
                            Para los planes de suscripción, trabajamos con tarjetas de crédito y debito de la red Bancard. Si te da inseguridad de utilizar tus tarjetas en la web, podes abonar en la app de Bancard PAGO MOVIL (disponible en Google Play y Apple Store), buscando el comercio Tilli e ingresando su número de CI.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="heading6">
                        <h2 class="mb-0">
                            <button class="btn-frecuente collapsed" type="button" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                                ¿Puedo elegir las fotos que voy a recibir?
                            </button>
                        </h2>
                    </div>
                    <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordionExample">
                        <div class="card-body">
                            Puedes, ingresas a la pagina una vez al mes a hacer upload de las fotos que quieres recibir, en caso de que no ingreses, el sistema selecciona las fotos automáticamente de tus redes sociales registradas en tu cuenta.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="heading6">
                        <h2 class="mb-0">
                            <button class="btn-frecuente collapsed" type="button" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                                ¿Cuál es el tamaño de las fotos?
                            </button>
                        </h2>
                    </div>
                    <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordionExample">
                        <div class="card-body">
                            Hay tres formatos: retrato (10x15 cm), paisaje (10x10 cm) y cuadrada (10x13 cm). Estos formatos son seleccionados automáticamente por nuestro sistema, de acuerdo al formato de su foto original.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="heading7">
                        <h2 class="mb-0">
                            <button class="btn-frecuente collapsed" type="button" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
                                ¿Como hago para agregar pie de foto a mi foto?
                            </button>
                        </h2>
                    </div>
                    <div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#accordionExample">
                        <div class="card-body">
                            Los pies de fotos son opcionales y debes llenarlo manualmente en tu ambiente de suscriptor de Tilli. Podes ingresar a tu cuenta, elegir la foto y agregar el pie de foto. Asimismo como emojis, cantidad de likes y fecha.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="heading8">
                        <h2 class="mb-0">
                            <button class="btn-frecuente collapsed" type="button" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">
                                ¿Puedo imprimir fotos por unidad en Tilli?
                            </button>
                        </h2>
                    </div>
                    <div id="collapse8" class="collapse" aria-labelledby="heading8" data-parent="#accordionExample">
                        <div class="card-body">
                            Por el momento solo trabajamos por suscripción. Dependiendo de la cantidad de fotos deseada, puedes optar por suscribirte por solamente un mes y cancelar la suscripción luego de haber recibido tu paquete. No tenemos un tiempo mínimo de permanencia o tasa de cancelación. Puedes cancelar tu suscripción ingresando a tu cuenta o enviando un email a hola@tilli.com.py.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="heading9">
                        <h2 class="mb-0">
                            <button class="btn-frecuente collapsed" type="button" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapse9">
                                ¿Cuándo recibiré mi primer pack de fotos?

                            </button>
                        </h2>
                    </div>
                    <div id="collapse9" class="collapse" aria-labelledby="heading9" data-parent="#accordionExample">
                        <div class="card-body">
                            El primer pack recibís después de 5 días de haber confirmado tu suscripción. Y a partir del segundo, ya se establece una fecha fija mensualmente, siempre te llegará en el mismo rango de fecha. Pues como somos un club de suscripción, tenemos fechas específicas al mes en que colectamos las fotos que subiste o hacemos el sorteo en caso de que no hayas subido fotos nuevas en tu cuenta en Tilli, luego impresión, montaje del paquete y envío para todos los suscriptores. Ni bien actives tu plan, recibirás um e-mail de confirmación con tus datos, dirección de entrega y la fecha que estarás recibiendo tu paquete. Luego de la colecta de fotos necesitamos aproximadamente 5 días laborales de preparación para así garantizar la calidad del paquete que recibirás. Cuando tu pack sea enviado vas a recibir un e-mail con el código de rastreo. El tiempo de entrega depende de tu dirección. Los próximos meses funcionarán de la misma forma mientras tu plan esté activado. Todos los eventos de colecta de fotos, cobranza y envío son informados por e-mail. Importante destacar que las cobranzas en tu tarjeta de crédito/débito son realizadas en la fecha de colecta de fotos (por supuesto, una vez al mes, casi siempre en la misma fecha).
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="heading10">
                        <h2 class="mb-0">
                            <button class="btn-frecuente collapsed" type="button" data-toggle="collapse" data-target="#collapse10" aria-expanded="false" aria-controls="collapse10">
                                ¿Puedo recibir fotos repetidas?
                            </button>
                        </h2>
                    </div>
                    <div id="collapse10" class="collapse" aria-labelledby="heading10" data-parent="#accordionExample">
                        <div class="card-body">
                            Solamente si la subis repetida. En caso de las que se sortean desde tus redes sociales, nunca te llegarán repetidas.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="heading11">
                        <h2 class="mb-0">
                            <button class="btn-frecuente collapsed" type="button" data-toggle="collapse" data-target="#collapse11" aria-expanded="false" aria-controls="collapse11">
                                ¿Solamente puedo suscribirme a Tilli si tengo una cuenta en Facebook o Instagram?
                            </button>
                        </h2>
                    </div>
                    <div id="collapse11" class="collapse" aria-labelledby="heading11" data-parent="#accordionExample">
                        <div class="card-body">
                            No necesariamente. Puedes crear tu cuenta en Tilli, suscribirte a uno de los planes y utilizar la función de upload de fotos. De esta forma, recibirás tus packs solamente con las fotos que hiciste upload desde tu celular, computadora o tablet.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="heading12">
                        <h2 class="mb-0">
                            <button class="btn-frecuente collapsed" type="button" data-toggle="collapse" data-target="#collapse12" aria-expanded="false" aria-controls="collapse12">
                                ¿Puedo modificar la dirección de entrega con mi suscripción activa?
                            </button>
                        </h2>
                    </div>
                    <div id="collapse12" class="collapse" aria-labelledby="heading12" data-parent="#accordionExample">
                        <div class="card-body">
                            Sí, es posible, desde que este sea modificado antes de la fecha de selección de fotos del mes. Esta modificación debe ser realizada directamente desde su ambiente de suscriptor en la pagina web. El valor del flete puede ser modificado dependiendo de la nueva dirección que ingreses.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="heading13">
                        <h2 class="mb-0">
                            <button class="btn-frecuente collapsed" type="button" data-toggle="collapse" data-target="#collapse13" aria-expanded="false" aria-controls="collapse13">
                                ¿Puedo modificar los datos de mi tarjeta de crédito/débito con mi suscripción activa?
                            </button>
                        </h2>
                    </div>
                    <div id="collapse13" class="collapse" aria-labelledby="heading13" data-parent="#accordionExample">
                        <div class="card-body">
                            Sí puedes. Para actualizar los datos de tu tarjeta de crédito/débito, debes ingresar a tu cuenta de Tilli, en ambiente de suscriptor, buscas la sección “Medio de Pago” en el menú y dale click en editar.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="heading14">
                        <h2 class="mb-0">
                            <button class="btn-frecuente collapsed" type="button" data-toggle="collapse" data-target="#collapse14" aria-expanded="false" aria-controls="collapse14">
                                ¿Tilli entrega en cualquier lugar?
                            </button>
                        </h2>
                    </div>
                    <div id="collapse14" class="collapse" aria-labelledby="heading14" data-parent="#accordionExample">
                        <div class="card-body">
                            Realizamos envíos a todo Paraguay, de puerta en puerta en Asunción y ciudades vecinas, en otras ciudades se lo enviamos por transportadora y puedes retirar del local disponible en su ciudad. No realizamos envíos internacionales.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="heading15">
                        <h2 class="mb-0">
                            <button class="btn-frecuente collapsed" type="button" data-toggle="collapse" data-target="#collapse15" aria-expanded="false" aria-controls="collapse15">
                                ¿Como funciona el programa de recomendación de Tilli?
                            </button>
                        </h2>
                    </div>
                    <div id="collapse15" class="collapse" aria-labelledby="heading15" data-parent="#accordionExample">
                        <div class="card-body">
                            Luego del primer mes de suscripción, recibirás un código de recomendación y acumulará 10.000 Gs. de descuento a cada persona que se suscriba a un plan de Tilli utilizando tu código. Y más, las personas a las que hayas recomendado también recibirán un descuento de 10.000 Gs. en su primer mes de suscripción.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 contacto">
               <center><p>Si aún te quedan dudas, por favor, envíanos tu pregunta. Te responderemos cuanto antes.</p></center>
               <center><p>O llamanos al +595 972 249-304.</p></center>
                <div class="form-group row">
                    <div class="col-sm-6">
                        <input type="text"  name="nombre" id="nombre" placeholder="Nombre" >
                    </div>
                     <div class="col-sm-6">
                        <input type="email" name="email" id="email" placeholder="Correo" >
                    </div>
                    <div class="col-sm-12">
                        <textarea rows="3" name="mensaje" id="mensaje" placeholder="Mensaje"></textarea>
                    </div>
                   <button onclick="javascript:enviar();">enviar</button>
                </div>
            </div>
        </div>
    </section>
    <div id="cover-spin"></div>

    <footer>
        <center>
            <p><?php echo date('Y')?> by Tilli Club</p>
        </center>
    </footer>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script src="../js/nav.js"></script>
    <script src='https://cdn.jsdelivr.net/npm/sweetalert2'></script>

    <script>
        AOS.init();

        function enviar() {
            if($('#nombre').val()==''){
                Swal.fire({
                    title: 'Error!',
                    text: 'Por favor complete el campo nombre',
                    type: 'error',
                    confirmButtonText: 'Ok!'
                });
                return;
            }else{
                if($('#email').val()==''){
                    Swal.fire({
                        title: 'Error!',
                        text: 'Por favor complete el campo email',
                        type: 'error',
                        confirmButtonText: 'Ok!'
                    });
                    return;
                }else{
                    if($('#mensaje').val()==''){
                        Swal.fire({
                            title: 'Error!',
                            text: 'Por favor escriba un mensaje',
                            type: 'error',
                            confirmButtonText: 'Ok!'
                        });
                        return;
                    }
                }
            }

            var urlSendMail = "<?php echo $servicio .'Clientes/enviarCorreo/'?>";
                var send = {
                    mensaje : 'Recibio un mensaje de ' + $('#nombre').val() +'<br>' + 'Mensaje: ' +$('#mensaje').val()+ '<br>'+ 'Email: ' + $('#email').val(),
                    titulo : 'Contacto desde Tilli'
                }

                $.ajax({
                    data : send,
                    url:   urlSendMail, //archivo que recibe la peticion
                    type:  'POST', //método de envio,
                    dataType:'json',
                    beforeSend: function () {
                        $('#cover-spin').show(0);
                    },
                    success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                        $('#cover-spin').hide(0);
                        if(response.response){
                            Swal.fire({
                                title: 'Exito!',
                                text: response.mensaje,
                                type: 'success',
                                confirmButtonText: 'Ok!'
                            })
                            $('#nombre').val('');
                            $('#mensaje').val('');
                            $('#email').val('');
                        }else{
                            Swal.fire({
                                title: 'Error!',
                                text: response.mensaje,
                                type: 'error',
                                confirmButtonText: 'Ok!'
                            })
                        }
                       

                    },
                    error: function (error){
                        $('#cover-spin').hide(0);
                        Swal.fire({
                            title: 'Error!',
                            text: error,
                            type: 'error',
                            confirmButtonText: 'Ok!'
                        })
                    }
                });

        }

    </script>
</body>

</html>
