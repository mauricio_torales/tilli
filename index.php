<?php 
session_start();
$_SESSION["url_servicio"] = getenv('HOSTTILLIADMIN');
$_SESSION["url_web"] = getenv('HOSTTILLIWEB');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With");
require_once('instagram/instagram-api-settings.php');
$login_url = 'https://api.instagram.com/oauth/authorize/?client_id=' . INSTAGRAM_CLIENT_ID . '&redirect_uri=' . urlencode(INSTAGRAM_REDIRECT_URI) . '&response_type=code&scope=basic';


?>
<!doctype html>
<html lang="es">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="favicon.ico" type="image/x-icon" />


    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- CSS -->
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <script src="https://kit.fontawesome.com/46ffcb8aaa.js"></script>
    <title>Tilli</title>
</head>

<body>
    <nav class="navbar fixed-top navbar-expand-lg navbar-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">
            <img class="logo" src="img/logo.webp" alt="logo">
        </a>

        <div class="collapse navbar-collapse" id="menu">
            <ul id="nav" class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#como-funciona">
                        <img src="img/como-funciona.png" alt="boton">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#suscribirme">
                        <img src="img/suscribirme.png" alt="boton">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="preguntas-frecuentes/index.php">
                        <img style=" width:190px!important;" src="img/frecuentes.png" alt="boton">
                    </a>
                </li>
                <?php if(isset($_SESSION['cliente'])):?>
                    <li class="nav-item">
                        <a class="nav-link" href="fotos/index.php">
                            <img src="img/fotos.svg" alt="fotos">
                        </a>
                    </li>
                <?php endif;?>
                <?php if(!isset($_SESSION['cliente'])):?>
                    <li class="nav-item">
                        <a class="nav-link" href="Javascript:Void(0)" data-toggle="modal" data-target="#login">
                            <img src="img/ingresar.png" alt="boton">
                        </a>
                    </li>
                <?php else:?>
                    <li class="nav-item">
                        <a class="nav-link" href="salir.php">
                            <img src="img/SALIR.svg" alt="Salir">
                        </a>
                    </li>
                <?php endif;?>
            </ul>
        </div>
    </nav>
    <div id="cover-spin"></div>
    <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="loginLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="container is-login" id="mainContainer">
                    <div class="card card--login shadow-2dp is-active" id="logInForm">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <form id="formLogin" name="formLogin">
                            <div class="card__content">
                                <a href="<?= $login_url ?>">
                                    <i class="fab fa-instagram log-insta"></i>
                                </a>
                                <a href="facebook/fbconfig.php">
                                    <i class="fab fa-facebook-square log-face"></i>
                                </a>
                                <div class="inputfield">
                                    <input class="inputfield__input" type="email" id="email" name="email" required="required">
                                    <label class="inputfield__label">Email</label>
                                    <span class="inputfield__underline"></span>
                                </div>
                                <div class="inputfield">
                                    <input class="inputfield__input" type="password" id="pass" name="pass" required="required">
                                    <label class="inputfield__label">Contraseña</label>
                                    <span class="inputfield__underline"></span>
                                </div>
                                <p style="color:red;display:none" id="errorLabel" class="text-center">Error</p>
                                <p class="text-center"><a href="#" data-toggle="form" data-target="amnesiaForm" data-type="amnesia">Olvido su contraseña?</a></p>
                            </div>
                            <div class="card__action">
                                <button class="btn btn--primary btn--block" type="submit" id="ingresar" name="ingresar">
                                    Ingresar
                                </button>
                            </div>
                        </form>
                        <div class="card__action">
                            <button class="btn btn--secondry btn--block" data-toggle="form" data-target="registerForm" data-type="register" type="button">
                                Registrarse
                            </button>
                        </div>
                    </div>

                    <div class="card card--amnesia shadow-2dp" id="amnesiaForm">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div class="card__content">
                            <div class="inputfield">
                                <input class="inputfield__input" type="email">
                                <label class="inputfield__label">Email</label>
                                <span class="inputfield__underline"></span>
                            </div>
                        </div>
                        <div class="card__action">
                            <button class="btn btn--flat btn--primary" type="button">
                                Recuperar contraseña
                            </button>
                            <button class="btn btn--flat" data-toggle="form" data-target="logInForm" data-type="login" type="button">
                                Cancelar
                            </button>
                        </div>
                    </div>
                    <div id="cover-spin"></div>

                    <div class="card card--register shadow-2dp" id="registerForm">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    <form id="formRegistro" name="formLogin">

                        <div class="card__content">
                            <a href="<?= $login_url ?>">                           
                                <i class="fab fa-instagram log-insta"></i>
                            </a>
                            <a href="Facebook/fbconfig.php">
                                <i class="fab fa-facebook-square log-face"></i>
                            </a>
                            <div class="inputfield">
                                <input class="inputfield__input" type="email" id="email_reg" name="email_reg" required="required">
                                <label class="inputfield__label">Email <i class="required">*</i></label>
                                <span class="inputfield__underline"></span>
                            </div>
                            <div class="inputfield">
                                <input class="inputfield__input" type="text" id="nombre_reg" name="nombre_reg" required="required">
                                <label class="inputfield__label">Nombre y Apellido <i class="required">*</i></label>
                                <span class="inputfield__underline"></span>
                            </div>
                            <div class="inputfield">
                                <input class="inputfield__input" type="password" id="pass_reg" name="pass_reg" required="required">
                                <label class="inputfield__label">Contraseña <i class="required">*</i></label>
                                <span class="inputfield__underline"></span>
                            </div>
                            <div class="inputfield">
                                <input class="inputfield__input" type="password" id="pass_two_reg" name="pass_two_reg" required="required">
                                <label class="inputfield__label">Repita su contraseña <i class="required">*</i></label>
                                <span class="inputfield__underline"></span>
                            </div>
                            <p style="color:red;display:none" id="errorLabelReg" class="text-center">Error</p>
                            <p class="text-center">Campos obligatorios <i class="required">*</i>.</p>
                        </div>
                        <div class="card__action">
                            <button class="btn btn--primary btn--block" type="submit" id="registrar" name="registrar">
                                Crear cuenta
                            </button>
                        </div>
                        </form>
                        <div class="card__action">
                            <button class="btn btn--secondry btn--block" data-toggle="form" data-target="logInForm" data-type="login" type="button">
                                Ingresar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="inicio">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <img id="x" src="img/x.svg" alt="">
                </div>
                <div class="col-md-8 pol-inicio">
                    <img id="ondas" src="img/ondas.svg" alt="">
                    <h1>INSTAN <br>
                        TÁ <br>
                        NEAS
                    </h1>
                    <h4>Fábrica de Memorias</h4>
                </div>
            </div>
        </div> 
    </section>
    <section class="planes" id="como-funciona">
        <center>
            <h2>Tilli es Simple,<br>
                Pero Diferente</h2>
        </center>
        <div class="row">
            <div class="col-md-3 garabato-uno" data-aos="fade-right" data-aos-duration="2000">
                <img src="img/garabato-1.png" alt="planes">
            </div>
            <div class="col-md-2" data-aos="fade-up" data-aos-duration="1000">
                <div class="img-planes">
                    <img src="img/estrella.png" alt="planes">
                </div>
                <h3>Elegí un plan</h3>
                <p>Conecta tu forma de pago eligiendo el pack con cuántas memorias quieres recibir mensualmente.</p>
            </div>
            <div class="col-md-2" data-aos="fade-up" data-aos-duration="1000">
                <div class="img-planes">
                    <img src="img/camara.png" alt="planes">
                </div>
                <h3>Envíe sus memorias</h3>
                <p>Envíanos las fotos que más te gustan que te tomaste en ese mes, se la imprimimos en formato Polaroid.</p>
            </div>
            <div class="col-md-2" data-aos="fade-up" data-aos-duration="1000">
                <div class="img-planes">
                    <img src="img/casa.png" alt="planes">
                </div>
                <h3>Reciba en Casa</h3>
                <p>Todas las fotos recibidas en nuestro sistema serán impresas y entregadas en la comodidad de tu casa.</p>
            </div>
            <div class="col-md-3 garabato-dos" data-aos="fade-left" data-aos-duration="2000">
                <img src="img/garabato-1.png" alt="planes">
            </div>
        </div>
    </section>

    <section class="suscribirse" id="suscribirme">
        <div class="row">
            <div class="col-md-6 cuadro-1" data-aos="fade-right" data-aos-duration="1000">
                <div class="pol-comprar">

                </div>
            </div>
            <div class="col-md-6 cuadro-2" data-aos="fade-left" data-aos-duration="1000">
                <div class="comprar" data-aos="fade-left" data-aos-duration="2000">
                    <h1>Elegí <br> tu <br> pack</h1>
                </div>
            </div>
            <div class="col-md-4 cuadro-3" data-aos="zoom-in-up" data-aos-duration="1000">
                <p>> 6 fotos polaroid mensuales 12.000 gs.</p>
                <?php if(isset($_SESSION['cliente'])):?>
                    <a class="nav-link" disabled href="javascript:suscripcion(<?php echo $_SESSION['cliente']["idcliente"] . ',1'; ?>);">
                        <img class="btn-pack" src="img/btn-sus-1.png" alt="">
                    </a>
                <?php else:?>
                    <a class="nav-link" disabled href="javascript:sidablebPop();">
                        <img class="btn-pack" src="img/btn-sus-1.png" alt="">
                    </a>
                <?php endif;?>
            </div>
            <div class="col-md-4 cuadro-4" data-aos="zoom-in-up" data-aos-duration="1000">
                <p>> 9 fotos polaroid mensuales 18.000 gs</p>
                <?php if(isset($_SESSION['cliente'])):?>
                <a class="nav-link" href="javascript:suscripcion(<?php echo $_SESSION['cliente']["idcliente"] . ',2'; ?>);">
                    <img class="btn-pack" src="img/btn-sus-2.png" alt="">
                </a>
                <?php else:?>
                <a class="nav-link" href="javascript:sidablebPop();">
                    <img class="btn-pack" src="img/btn-sus-2.png" alt="">
                </a>
                <?php endif;?>
            </div>
            <div class="col-md-4 cuadro-5" data-aos="zoom-in-up" data-aos-duration="1000">
                <p>> 12 fotos polaroid mensuales 25.000 gs.</p>
                <?php if(isset($_SESSION['cliente'])):?>
                <a class="nav-link" href="javascript:suscripcion(<?php echo $_SESSION['cliente']["idcliente"] . ',3'; ?>);">
                    <img class="btn-pack" src="img/btn-sus-1.png" alt="">
                </a>
                <?php else:?>
                <a class="nav-link" href="javascript:sidablebPop();">
                    <img class="btn-pack" src="img/btn-sus-1.png" alt="">
                </a>
                <?php endif;?>
            </div>
        </div>
    </section>
    <section class="instagram" id="instagram">
        <h1>Siguenos</h1>
        <a href="">
            <h2>@tilli.club</h2>
        </a>
        <div class="container">
            <div class="row" id="insta">

            </div>
        </div>
    </section>
    <section class="news">
        <div class="row">
            <div class="col-md-6 formulario">
                <img class="ondas-2" src="img/ondas.svg" alt="ondas" data-aos="zoom-in-up" data-aos-duration="1000">
                <div class="cuadro-rosa">
                    <h1>Stay in <br>
                        the Loop</h1>
                    <p>Únete a nuestra lista de correos <span> No te pierdas nada</span></p>
                    <input type="text" class="form-control email" placeholder="Email">
                    <img class="btn-recibir" src="img/btn-recibir.png" alt="recibir">
                </div>
                <img class="x-2" src="img/x.png" alt="x" data-aos="zoom-in-left" data-aos-duration="2000">
            </div>
            <div class="col-md-6 formulario-2">

            </div>
        </div>
    </section>


   
    <footer style="margin-top: 0px;">
        <center>
            <p><?php echo date('Y')?> by Tilli Club</p>
        </center>
    </footer>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script src="js/nav.js"></script>
    <script src="js/login.js"></script>
    <script>
        AOS.init();
        $(document).ready(function() {
            $("#x").hide(0).delay(1000).fadeIn(4000);
            $("#ondas").show();
        });
        $(document).ready(function() {
            $('#nav').onePageNav();
        });
        var name = "tilli.club",
            items;
        $.get("https://images" + ~~(Math.random() * 33) + "-focus-opensocial.googleusercontent.com/gadgets/proxy?container=none&url=https://www.instagram.com/" + name + "/", function(html) {
            if (html) {
                var regex = /_sharedData = ({.*);<\/script>/m,
                    json = JSON.parse(regex.exec(html)[1]),
                    edges = json.entry_data.ProfilePage[0].graphql.user.edge_owner_to_timeline_media.edges;
                $.each(edges, function(n, edge) {
                    var node = edge.node;
                    $("#insta").append('<div class="col-md-4" data-aos="fade-up" data-aos-duration="1000"><img width="100%"  src="' + node.thumbnail_src + '"></div>');
                });
            }
        });

    </script>
</body>


</html>



<script src='https://cdn.jsdelivr.net/npm/sweetalert2'></script>

<script>
var urlAdmin = '<?php echo $_SESSION["url_servicio"]?>';
// var urlPage = "http://localhost/tilliV2/";
var urlPage = '<?php echo $_SESSION["url_web"]?>';
</script>


<script src="js/funciones.js"></script>


<script>
        var id_cliente = '<?php echo $_SESSION['cliente']['idcliente']?>'
        getDataCLiente(id_cliente);

    </script>