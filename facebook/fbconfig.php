<?php
session_start();
// added in v4.0.0
require_once 'autoload.php';
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;
$host = getenv('HOSTTILLIADMIN');
$hostweb = getenv('HOSTTILLIWEB');
// init app with app id and secret
FacebookSession::setDefaultApplication( '972949543056619','f5f21ee32f306686edd022a8523c6d09' );
// login helper with redirect_uri
    $helper = new FacebookRedirectLoginHelper($hostweb.'facebook/fbconfig.php' );
try {
  $session = $helper->getSessionFromRedirect();
} catch( FacebookRequestException $ex ) {
  // When Facebook returns an error
} catch( Exception $ex ) {
  // When validation fails or other local issues
}
$url = $host."users/loginFacebook";

// see if we have a session
if ( isset( $session ) ) {

  // graph api request for user data
  $request = new FacebookRequest( $session, 'GET', '/me?fields=name,email,picture' );
  $response = $request->execute();
  // get response
  $graphObject = $response->getGraphObject();
  $fbid = $graphObject->getProperty('id');              // To Get Facebook ID
  $fbfullname = $graphObject->getProperty('name'); // To Get Facebook full name

  $femail = $graphObject->getProperty('email');    // To Get Facebook email ID
  $imagen = $graphObject->getProperty('picture')->getProperty('url');    // To Get Facebook email ID


  $data = json_encode(array(
    "Fuid" => $fbid,
    "nombre_completo" => $fbfullname,
    "username" => $femail,
    "email"=> $femail,
    "imagen" =>$imagen
  ));



  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $result = curl_exec($ch);

  $result = json_decode($result, false);
  $result = json_decode(json_encode($result), true);

  $cliente = $result['cliente'];        
  $_SESSION['cliente'] = $cliente;

  curl_close($ch);

  header("Location: ../fotos/index.php");
} else {
  $loginUrl = $helper->getLoginUrl();
 header("Location: ".$loginUrl);
}
?>