<?php 
    session_start();
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST');
    header("Access-Control-Allow-Headers: X-Requested-With");

?>

<?php if(!empty($_SESSION)):?>  

<!doctype html>
<html lang="es">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="../favicon.ico" type="image/x-icon" />


    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- CSS -->
    <link rel="stylesheet" href="../css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <script src="https://kit.fontawesome.com/46ffcb8aaa.js"></script>

    <link rel="stylesheet" href="../cropper/cropper/cropper.min.css">
    <link rel="stylesheet" href="../cropper/dropzone/basic.css">
    <link rel="stylesheet" href="../cropper/dropzone/dropzone.css">
    <link rel="stylesheet" href="../cropper/font-awesome/css/font-awesome.min.css">


    <title>Tilli</title>
</head>

<body>
    <nav class="navbar fixed-top navbar-expand-lg navbar-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="../index.php">
            <img class="logo" src="../img/logo.webp" alt="logo">
        </a>

        <div class="collapse navbar-collapse" id="menu">
            <ul id="nav" class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="../cuenta/index.php">
                        <img src="../img/cuenta.svg" alt="fotos">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../salir.php">
                        <img src="../img/SALIR.svg" alt="salir">
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <section class="mis-fotos">
        <div class="container">
            <div class="row">
                <div class="col-md-3 panel-izquierdo d-block d-sm-none">
                    <h2><?php echo $_SESSION['cliente']['nombre_completo']?></h2>
                    <div class="separador"></div>
                    <h3 id="plan_name">PLAN</h3>
                    <br>
                    <p><i class="fas fa-camera-retro"></i><label id="cantidad_fotos_none">0</label> Fotos</p>
                    <p><i class="fas fa-check-square"></i> <label id="cantidad_fotos_seleccionadas_none">0</label> Fotos seleccionadas</p>
                    <div class="separador"></div>
                    <h4>Agregar fotos</h4>
                    <button onclick="javascript:instagranFotos();" class="btn-instagram"><i class="fab fa-instagram"></i> INSTAGRAM</button><br>
                    <button onclick="javascript:facebookFotos(<?php echo $_SESSION['cliente']['idcliente'];?>);" class="btn-facebook"><i class="fab fa-facebook-square"></i> FACEBOOK</button><br>
                    <button onclick="javascript:subirFotos();" class="btn-subir"><i class="fas fa-upload"></i> SUBIR FOTOS</button>
                </div>          
                <div class="col-md-9 panel-derecho">
                    <h1>Mis memorias</h1>
                    <div class="row justify-content-md-center" id="fotos_div">

                    </div>
                </div>
                <div class="col-md-3 panel-izquierdo d-none d-sm-block">
                    <h2><?php echo $_SESSION['cliente']['nombre_completo']?></h2>
                    <div class="separador"></div>
                    <h3 id="plan_name">PLAN</h3>
                    <br>
                    <p><i class="fas fa-camera-retro"></i> <label id="cantidad_fotos_block">0</label> Fotos</p>
                    <p><i class="fas fa-check-square"></i> <label id="cantidad_fotos_seleccionadas_block">0</label> Fotos seleccionadas</p>
                    <div class="separador"></div>
                    <h4>Agregar fotos</h4>
                    <button onclick="javascript:instagranFotos();" class="btn-instagram"><i class="fab fa-instagram"></i> INSTAGRAM</button><br>
                    <button onclick="javascript:facebookFotos(<?php echo $_SESSION['cliente']['idcliente'];?>);" class="btn-facebook"><i class="fab fa-facebook-square"></i> FACEBOOK</button><br>
                    <button onclick="javascript:subitFotos()" class="btn-subir"><i class="fas fa-upload"></i> SUBIR FOTOS</button>
                   <br> <p style='text-align: center;' id='pvale'>
                        Usted cuenta con un vale de descuento promocional, valida <label id="valeText"></label> <label id="vecesText">veces</label>
                        <br><label>CODIGO:</label> <label id='codigoValeText'></label>
                        </p>
                </div>
            </div>

        </div>
    </section>
    <div id="cover-spin"></div>

  
  

    <footer>
        <center>
            <p><?php echo date('Y')?> by Tilli Club</p>
        </center>
    </footer>

      <!-- Modal agregar foto -->
      <div class="modal fade" id="modal-fotos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-tarjeta">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Subir Fotos</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <main>
                    <form action="../cropper/upload.php?cliente=<?php echo $_SESSION['cliente']['idcliente']?>" class="dropzone" id="my-dropzone-container" method="post" enctype="multipart/form-data" style="width: 100%;">
                        <div class="fallback">
                            <input name="file" type="file">
                        </div>
                    </form>
                </main>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script src="../js/nav.js"></script>
    <script>
var urlAdmin = '<?php echo $_SESSION["url_servicio"]?>';
// var urlPage = "http://localhost/tilliV2/";
var urlPage = '<?php echo $_SESSION["url_web"]?>';
</script>
    <script src="../js/funciones.js"></script>
    <script src='https://cdn.jsdelivr.net/npm/sweetalert2'></script>
    <script src="../cropper/cropper/cropper.min.js"></script>
    <script src="../cropper/dropzone/dropzone.js"></script>


    <script>
        AOS.init();
        
        var id_cliente = '<?php echo $_SESSION['cliente']['idcliente']?>'
        getDataCLiente(id_cliente);


    </script>
</body>

</html>
<style>

.dropzone {
    margin: 0 auto;
    border: 3px dashed #247bbb;
    height: 500px;
    width: 1000px;
}


</style>



<script>

var urlPage = '<?php echo $_SESSION["url_web"]?>';

// transform cropper dataURI output to a Blob which Dropzone accepts
var dataURItoBlob = function (dataURI) {
    var byteString = atob(dataURI.split(',')[1]);
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], {type: 'image/jpeg'});
};

Dropzone.autoDiscover = false;
var c = 0;

var myDropzone = new Dropzone("#my-dropzone-container", {
    addRemoveLinks: true,
    parallelUploads: 10,
    uploadMultiple: false,
    dictDefaultMessage: "Arrastre aqui sus imágenes",
    dictRemoveFile:"Quitar esta imagen",
    maxFiles: 10,
    init: function () {
        this.on('success', function (file) {
            var $button = $('<a href="#" class="js-open-cropper-modal" data-file-name="' +file.name + '">Cortar&Guardar</a>');
            $(file.previewElement).append($button);
            getDataCLiente(clienteData.cliente.idcliente);
        });
    }
});

$('#my-dropzone-container').on('click', '.js-open-cropper-modal', function (e) {
    e.preventDefault();
    var fileName = $(this).data('file-name');
    fileName=fileName.replace(/ /g,"-");
    console.log(fileName)
    var modalTemplate = '<div class="modal fade" id="modal-subit-fotos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">'+
        '<div class="modal-dialog" role="document">'+
            '<div class="modal-content modal-tarjeta">'+
               ' <div class="modal-header">'+
                    '<h5 class="modal-title" id="exampleModalLabel">Cortar Foto</h5>'+
                    '<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
                        '<span aria-hidden="true">&times;</span>'+
                    '</button>'+
                '</div>'+
                '<div class="modal-body">'+
                '<div class="image-container">' +
                    '<img id="img-' + ++c + '" src='+urlPage+'cropper/uploads/' + id_cliente +'-'+fileName + '>' +
                '</div>'+
               ' </div>'+
                '<div class="modal-footer">' +
                    '<button type="button" class="btn btn-warning rotate-left"><span class="fa fa-rotate-left"></span></button>' +
                    '<button type="button" class="btn btn-warning rotate-right"><span class="fa fa-rotate-right"></span></button>' +
                    '<button type="button" class="btn btn-warning scale-x" data-value="-1"><span class="fa fa-arrows-h"></span></button>' +
                    '<button type="button" class="btn btn-warning scale-y" data-value="-1"><span class="fa fa-arrows-v"></span></button>' +
                    '<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>' +
                    '<button type="button" class="btn btn-primary crop-upload">Cortar y guardar</button>' +
                '</div>' +
            '</div>'+
        '</div>'+
    '</div>';

        

        
                   

    var $cropperModal = $(modalTemplate);

    $cropperModal.modal('show').on("shown.bs.modal", function () {
        var cropper = new Cropper(document.getElementById('img-' + c), {
            autoCropArea: 1,
            movable: false,
            cropBoxResizable: true,
            rotatable: true
        });
        var $this = $(this);
        $this
            .on('click', '.crop-upload', function () {
                // get cropped image data
                var blob = cropper.getCroppedCanvas().toDataURL();
                // transform it to Blob object
                var croppedFile = dataURItoBlob(blob);
                croppedFile.name = fileName;

                var files = myDropzone.getAcceptedFiles();
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    if (file.name === fileName) {
                        myDropzone.removeFile(file);
                    }
                }
                myDropzone.addFile(croppedFile);
                $this.modal('hide');
            })
            .on('click', '.rotate-right', function () {
                cropper.rotate(90);
            })
            .on('click', '.rotate-left', function () {
                cropper.rotate(-90);
            })
            .on('click', '.reset', function () {
                cropper.reset();
            })
            .on('click', '.scale-x', function () {
                var $this = $(this);
                cropper.scaleX($this.data('value'));
                $this.data('value', -$this.data('value'));
            })
            .on('click', '.scale-y', function () {
                var $this = $(this);
                cropper.scaleY($this.data('value'));
                $this.data('value', -$this.data('value'));
            });
    });
});
</script>
<?php else:?>




<?php
  header("Location: ../");
?>


<?php endif;?>



