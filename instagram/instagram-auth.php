<?php
session_start();

require_once('instagram-api-settings.php');
require_once('instagram-login-api.php');
$host=getenv('HOSTTILLIADMIN');
$url = $host."users/loginInstagram";

// Instagram passes a parameter 'code' in the Redirect Url
if(isset($_GET['code'])) {
	try {
			$instagram_ob = new InstagramApi();
			
			// Get the access token 
			$access_token = $instagram_ob->GetAccessToken(INSTAGRAM_CLIENT_ID, INSTAGRAM_REDIRECT_URI, INSTAGRAM_CLIENT_SECRET, $_GET['code']);
			//var_dump($access_token);die;
			// Get user information
			$user_info = $instagram_ob->GetUserProfileInfo($access_token);

			$data = json_encode(array(
				"Iuid" => $user_info['id'],
				"nombre_completo" => $user_info['full_name'],
				"username" => $user_info['username'],
				"email"=> $user_info['username'],
				"imagen" =>$user_info['profile_picture']
			));


			
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);

			
			$result = json_decode($result, false);
			$result = json_decode(json_encode($result), true);
			
			$cliente = $result['cliente'];        
			$_SESSION['cliente'] = $cliente;
			
			curl_close($ch);
			
			header("Location: ../fotos/index.php");
		
	}
	catch(Exception $e) {
		echo $e->getMessage();
		exit;
	}
}

?>