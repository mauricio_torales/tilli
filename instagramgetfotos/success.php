<?php
session_start();
/**
 * Instagram PHP API
 *
 * @link https://github.com/cosenary/Instagram-PHP-API
 * @author Christian Metz
 * @since 01.10.2013
 */
require 'src/Instagram.php';
use MetzWeb\Instagram\Instagram;
$host=getenv('HOSTTILLIADMIN');
$hostweb=getenv('HOSTTILLIWEB');
$url = $host."clienteFotos/insertFotos/instagram/".$_SESSION['cliente']['idcliente'];
// initialize class
$instagram = new Instagram(array(
    'apiKey' => '33525320fa3e444499a2a50ec2267df5',
    'apiSecret' => '06bdef22d1f34fd99a86c89b4855eace',
    'apiCallback' => $hostweb.'instagramgetfotos/success.php' // must point to success.php
));
// receive OAuth code parameter
$code = $_GET['code'];
// check whether the user has granted access
if (isset($code)) {
    // receive OAuth token object
    $data = $instagram->getOAuthToken($code);
$username = $data->user->username;
    // store user access token
    $instagram->setAccessToken($data);
    // now you have access to all authenticated user methods
    $photos = $instagram->getUserMedia('self', 200);
    $imagenes = []; 
    $keyImagen=0;
    foreach ($photos->data as $media) {
        if ($media->type != 'video') {
            $imagenes[$keyImagen]['rutaImagen'] = $media->images->standard_resolution->url;
            $imagenes[$keyImagen]['cliente_id'] = $_SESSION['cliente']['idcliente'];
            $imagenes[$keyImagen]['proveedor'] = 'instagram';
            $imagenes[$keyImagen]['impreso'] = 0;
        }
        $keyImagen++;
    }

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($imagenes));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    $result = json_decode($result, false);
    $result = json_decode(json_encode($result), true);
   
    
    curl_close($ch);
    
    header("Location: ../fotos/index.php");

    
} else {
    // check whether an error occurred
    if (isset($_GET['error'])) {
        echo 'An error occurred: ' . $_GET['error_description'];
    }
}
?>
