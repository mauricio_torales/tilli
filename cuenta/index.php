<?php 
//esto luego hay que poner en el login, ahora esta en duro para desarrollo
session_start();
?>
<?php if(!empty($_SESSION)):?>

<?php 
//esto luego hay que poner en el login, ahora esta en duro para desarrollo
session_start();
// $_SESSION["url_servicio"]='http://localhost:8080/';
$clienteID = $_SESSION['cliente']["idcliente"];
$servicio = $_SESSION["url_servicio"];
$userEmail = $_SESSION['cliente']["email"];

$userTelefono = $_SESSION['cliente']["telefono"];
if(empty($userTelefono)){
    $userTelefono = '021';
}
// var_dump($_SESSION);die;


?>


<!doctype html>
<html lang="en">

<head>

<script>
        
    var placeSearch, autocomplete;
        var componentForm = {
            inputDireccionCalle: 'short_name',
        };

    function initAutocomplete() {
    // Create the autocomplete object, restricting the search predictions to
    // geographical location types.
    autocomplete = new google.maps.places.Autocomplete(
        document.getElementById('autocomplete'), {types: ['geocode'], componentRestrictions: { country: 'py' }});

    // Avoid paying for data that you don't need by restricting the set of
    // place fields that are returned to just the address components.
   // autocomplete.setFields(['address_component']);

    // When the user selects an address from the drop-down, populate the
    // address fields in the form.
    autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
    // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();


        for (var component in componentForm) {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
        }

        map.setCenter(place.geometry.location.lat(), place.geometry.location.lng());
            var latlng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
            positionCustomer.setPosition(latlng);
            cargarInput(place.geometry.location.lat(), place.geometry.location.lng());

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var val = place.address_components[i]["long_name"];
                if (addressType == "route") {
                    $('#inputDireccionCalle').val(place.address_components[i]["long_name"]);
                }

            }

        
    }

        // Bias the autocomplete object to the user's geographical location,
        // as supplied by the browser's 'navigator.geolocation' object.
        function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle(
                {center: geolocation, radius: position.coords.accuracy});
            autocomplete.setBounds(circle.getBounds());
            });
        }
        }
    </script>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="../favicon.ico" type="image/x-icon" />


    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- CSS -->
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/select2.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOzs_Te0C0fhCfulT7JPWee2WvNr14ws0&libraries=places&callback=initAutocomplete"
        async defer></script>
    <script src="../js/bancard-checkout-2.1.0-sandbox.js"></script>
    <title>Tilli</title>
</head>

<body>
    <nav class="navbar fixed-top navbar-expand-lg navbar-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="../index.php">
            <img class="logo" src="../img/logo.webp" alt="logo">
        </a>

        <div class="collapse navbar-collapse" id="menu">
            <ul id="nav" class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="../fotos/index.php">
                        <img src="../img/fotos.svg" alt="fotos">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../salir.php">
                        <img src="../img/SALIR.svg" alt="Salir">
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <section class="inicio">
        <div class="container">
            <div class="row">
                <div class="col-md-12 datos">
                    <h1>Vista general de la cuenta</h1>
                    <div class="row">
                        <div class="col-md-5 info">
                            <center>
                                <h4>Cuenta</h4>
                            </center>
                            <p>Nombre y Apellido: <span><label id="nombreText"></label></span> </p>
                            <p>Email: <span><label id="emailText"></label></span> </p>
                            <p>Telefono: <span><label id="telefonoText"></label></span> </p>
                            <p>Nro. Documento: <span><label id="documentoText"></label></span> </p>
                            <img id="btn-editar-cuenta" width="150" src="../img/editar.svg" alt="editar">
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-5 info">
                            <center>
                                <h4>dirección</h4>
                            </center>
                            <p>Ciudad: <span><label id="ciudadText"></label></span> </p>
                            <p>Barrio: <span><label id="barrioText"></label></span> </p>
                            <p>Calle principal: <span><label id="calle1Text"></label> <span><label id="nroCasaText"></label></span> </p>
                            <p>Calle secundaria: <span><label id="calle2Text"></label></span> </p>
                            <p>Referencia: <span><label id="referenciaText"></label></span> </p>
                            <img id="btn-editar-direccion" width="150" src="../img/editar.svg" alt="editar">
                        </div>
                    </div>
                </div>
                <div class="col-md-12 suscripto">
                    <h4>Plan actual y forma de pago.</h4>
                    <div class="row">
                        <div class="col-md-4 cuadro-3" data-aos="zoom-in-up" data-aos-duration="1000">
                        </div>
                        <div class="col-md-8">
                            <p id='psuscripcion'>Actualmente esta suscripto al plan <label id="planText"></label> de Gs. <label id="precioText">0</label></p>
                            <p id='pvale'>Usted cuenta con un vale de descuento promocional, valida <label id="valeText"></label> <label id="vecesText">veces</label>
                            <br><label>CODIGO:</label> <label id='codigoValeText'></label></p>
                            <p id='psuscripcion2' ><label>Esto se le debitara el <label id="planDiaText"></label> de cada mes.</p>
                            <button id="buttonCancelar" onclick="javascript:cancelarSuscripcion(<?php echo $_SESSION['cliente']['idcliente']?>)">Cancelar plan</button>
                            <p style="margin-top: 30px;">Sus medios de pagos disponibles son:</p>
                            <div class="row" id="tarjetasJson">
                                <!--<div class="col-md-5 tarjeta">
                                    <div class="franja"></div>
                                    <p>Visa **** 1156</p>
                                    <small>Vencimiento: 10/2019</small>
                                    <button>eliminar</button>
                                </div>-->
                            </div>
                            <button onclick="newCard()">Agregar medio de pago</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer style="margin-top: 0px;">
        <center>
            <p><?php echo date('Y')?> by Tilli Club</p>
        </center>
    </footer>
    <!-- Modal agregar tarjeta -->
    <div class="modal fade" id="modal-tarjeta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-tarjeta">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregue su tarjeta</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div style="height: 100%; width: 100%; margin: auto" id="iframe-container">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-cuenta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-tarjeta">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Editar datos de la cuenta</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Nombre y Apellido: <input required class = "form-control" type="text" id="inputCuentaNombre" value="<?php echo $_SESSION['cliente']['nombre_completo']?>"> </p>
                    <p>Email: <input required class = "form-control" type="text" id="inputCuentaEmail" value="<?php echo $_SESSION['cliente']['email']?>"> </p>
                    <p>Teléfono: <input class = "form-control" type="text" id="inputCuentaTelefono" value="<?php echo $_SESSION['cliente']['telefono']?>"> </p>
                    <p>Nro. Documento: <input class = "form-control" type="text" id="inputCuentaDocumento" value="<?php echo $_SESSION['cliente']['nro_documento']?>"> </p>
                    <a class="nav-link" href="Javascript:editUser(<?php echo $_SESSION['cliente']['idcliente']?>);">
                        <img id="btn-editar-cuenta" width="150" src="../img/editar.svg" alt="editar">
                    </a>
                </div>
                
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-direccion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-tarjeta">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Editar direcciones</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Ciudad: <br><div class="col-lg-12 col-md-12 inputs"><select name="select" class="form-control" id="selectCiudad" required>
                        </select> </p></div>
                    <p>Barrio: <input class="form-control" type="text" id="inputDireccionBarrio"> </p>
                    <p>Calle principal: <input class="form-control" type="text" id="inputDireccionCalle"></p>
                    <p>Calle secundaria: <input class="form-control" type="text" id="inputDireccionCalle2"> </p>
                    <p>Nro Casa: <input class="form-control" type="text" id="inputDireccionNroCasa"> </p>
                    <p>Referencia: <input class="form-control" type="text" id="inputDireccionReferencia"></span> </p>
                    <div style="display: inline-flex;    margin-left: -20px;">
                        <a class="nav-link" href="Javascript:addEditAddress();">
                            <img id="btn-editar-direccion" width="150" src="../img/editar.svg" alt="editar">
                        </a>
                        <a class="nav-link" href="javascript:abrirMapa();">
                            <img id="btn-editar-direccion" width="150" src="../img/mapa.png" alt="editar">
                        </a>
                </div>
                </div>
            </div>
        </div>
    </div>
    <div id="cover-spin"></div>


   <!-- Modal mapa -->
   <div class="modal fade" id="modal-mapas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-tarjeta">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Marcar en el mapa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                 <article class="" id="section-ubicacion">
                            <div class="col-lg-12 col-md-12 inputs">
 
                            <input id="autocomplete" class="form-control"
                                    placeholder="Busque su direccion"
                                    onFocus="geolocate()"
                                    type="text"/>

                            </div>
                            <br>
                            <div class="clear"></div>
                            <div id="cobertura-map" style="position: relative;width: auto;height: 500px;overflow: hidden;position: relative;
                width: 300px;
                height: 300px;"></div>

                    <input type="text" id="Lat" hidden>
                    <input type="text" id="Lng" hidden>
         
            </article>

                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script src="../js/nav.js"></script>
    <script>
var urlAdmin = '<?php echo $_SESSION["url_servicio"]?>';
// var urlPage = "http://localhost/tilliV2/";
var urlPage = '<?php echo $_SESSION["url_web"]?>';
</script>
    <script src="../js/funciones.js"></script>
    <script>
        AOS.init();
        $("#btn-editar-cuenta").click(function() {
            $("#modal-cuenta").modal();
        });
        $("#btn-editar-direccion").click(function() {
            $("#modal-direccion").modal();
        });

    </script>

    <!--area de carga de tarjetas-->
    <script>
        var urlGetCards = "<?php echo $servicio .'Clientes/tarjetasJsonWeb/' . $clienteID ?>";
        var clienteID = "<?php echo $clienteID ?>";
        function getCards(idCard) {
            $.ajax(
                urlGetCards, {
                    beforeSend: function() {
                        $('#cover-spin').show(0)
                    },
                    success: function(tarjetas) {
                        //    if(!isEmpty(tarjetas)){ 
                        $("#tarjetasJson").html("");
                        var cards = tarjetas;

                        for (let i = 0; i < cards.length; i++) {
                            var tipo = 'Débito';
                            var predeterminado = 'No';
                            if (cards[i].card_type == 'credit') {
                                tipo = 'Crédito';
                            }

                            if (cards[i].predeterminado) {
                                predeterminado = 'Si';
                            }
                            $("#tarjetasJson").append(' <div class="col-md-5 tarjeta"><div class="franja"></div><p>' +
                                cards[i].card_brand + ' ' + cards[i].card_masked_number + '</p>' +
                                '<small>Tipo: ' + tipo + '</small><br>' +
                                '<small>Predeterminado: ' + predeterminado + '</small>' +

                                '<button onclick="deleteCard(' + cards[i].idcliente_tarjetas + ', ' + clienteID + ', ' + cards[i].alias_token + ')">eliminar</button><button  onclick="predeterminado(' + cards[i].idcliente_tarjetas + ',' + clienteID + ')" style="color:;background-color: darkgoldenrod;">marcar como predeterminado</button></div>');

                        }
                        //   }
                        $('#cover-spin').hide(0)
                    },
                    error: function(error) {
                        $('#cover-spin').hide(0)
                        Swal.fire({
                            title: 'Error!',
                            text: 'Hubo un problema al recuperar las tarjetas',
                            type: 'error',
                            confirmButtonText: 'Ok!'
                        });
                    }
                }
            );
        }

        function deleteCard(idCard, idCliente, aliasToken) {
            var urlDeleteCards = "<?php echo $servicio .'ClienteCards/deleteCardJson/'?>";
            urlDeleteCards = urlDeleteCards + idCard + '/' + idCliente + '/' + aliasToken;
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Desea eliminar esta tarjeta?',
                text: "No lo podrá revertir!",
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Sí, elimianr!',
                cancelButtonText: 'No, cancelar!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    $.ajax(
                        urlDeleteCards, {
                            beforeSend: function() {
                                $('#cover-spin').show(0)
                            },
                            success: function(data) {
                                if (data.response == false) {
                                    Swal.fire({
                                        title: 'Error!',
                                        text: data.mensaje,
                                        type: 'error',
                                        confirmButtonText: 'Ok!'
                                    })
                                }
                                getCards();
                                $('#cover-spin').hide(0)
                            },
                            error: function() {
                                $('#cover-spin').hide(0)
                                Swal.fire({
                                    title: 'Error!',
                                    text: 'No se pudo eliminar la tarjeta',
                                    type: 'error',
                                    confirmButtonText: 'Ok!'
                                })

                            }
                        }
                    );
                }
            })
        }

        function predeterminado(idTarjeta, idCliente) {
            var urlPredeterminado = "<?php echo $servicio .'ClienteCards/marcarPredeterminadoCardJson/'?>";
            urlPredeterminado = urlPredeterminado + idTarjeta + '/' + idCliente
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })
            swalWithBootstrapButtons.fire({
                title: 'Desea marcar esta tarjeta?',
                text: "Puede marcar otro mas tarde!",
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Sí, marcar!',
                cancelButtonText: 'No, cancelar!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    $.ajax(
                        urlPredeterminado, {
                            beforeSend: function() {
                                $('#cover-spin').show(0)
                            },
                            success: function(data) {
                                if (data.response == false) {
                                    Swal.fire({
                                        title: 'Error!',
                                        text: data.mensaje,
                                        type: 'error',
                                        confirmButtonText: 'Ok!'
                                    })
                                }
                                getCards();
                                $('#cover-spin').hide(0)

                            },
                            error: function() {
                                $('#cover-spin').hide(0)
                                Swal.fire({
                                    title: 'Error!',
                                    text: 'No se pudo marcar la tarjeta',
                                    type: 'error',
                                    confirmButtonText: 'Ok!'
                                })
                            }
                        }
                    );
                }
            })

        }

        function newCard() {
            let timerInterval
            Swal.fire({
            title: 'Antes de agregar su tarjeta, tengo en cuenta',
            html: 'Estimado usuario, A continuación usted pasará por única vez por un proceso de validación con preguntas de seguridad. <br>Para iniciar favor tener en cuenta las siguientes recomendaciones: <li> Verifique su número de cédula de identidad</li> <li> Tenga a mano sus tarjetas de crédito y débito activas</li> <li> Verifique el monto y lugar de sus últimas compras en comercios o extracciones en cajeros</li>',
            timer: 10000,
            onBeforeOpen: () => {
                Swal.showLoading()
                timerInterval = setInterval(() => {
                Swal.getContent().querySelector('strong')
                    .textContent = Swal.getTimerLeft()
                }, 100)
            },
            onClose: () => {
                clearInterval(timerInterval)
            }
            }).then((result) => {
            if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.timer
            ) {
                var urlNewCard = "<?php echo $servicio .'ClienteCards/addNewCardJson/' . $clienteID . '/' . $userEmail .'/'. $userTelefono?>";
            styles = {
                "form-background-color": "#001b60",
                "button-background-color": "#4faed1",
                "button-text-color": "#fcfcfc",
                "button-border-color": "#dddddd",

                "input-background-color": "#fcfcfc",
                "input-text-color": "#111111",
                "input-placeholder-color": "#111111"
            };
            $.ajax(
                urlNewCard, {
                    beforeSend: function() {
                        $('#cover-spin').show(0)
                    },
                    success: function(data) {
                        // var response = JSON.parse(data);
                        if (data.response == false) {
                            Swal.fire({
                                title: 'Error!',
                                text: data.mensaje,
                                type: 'error',
                                confirmButtonText: 'Ok!'
                            })
                        }
                        $("#modal-tarjeta").modal();
                        Bancard.Cards.createForm('iframe-container', data.response, styles);
                        getCards();
                        $('#cover-spin').hide(0)

                    },
                    error: function() {
                        $('#cover-spin').hide(0)
                        alert('Hubo un problema al agregar la tarjeta');
                    }
                }
            );
            }
            })
           
        }


        getCards();


        function cancelarSuscripcion(idCliente) {
            var urlCancelar = "<?php echo $servicio .'suscripciones/cancelarSuscripcionJson/'?>";
            urlCancelar = urlCancelar + '/' + idCliente
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })
            swalWithBootstrapButtons.fire({
                title: 'Desea cancelar esta suscripción?',
                text: "Puede contratar otro más tarde!",
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Sí, cancelar!',
                cancelButtonText: 'No, cancelar!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    $.ajax(
                        urlCancelar, {
                            beforeSend: function() {
                                $('#cover-spin').show(0)
                            },
                            success: function(data) {
                                if (data.response == false) {
                                    Swal.fire({
                                        title: 'Error!',
                                        text: data.mensaje,
                                        type: 'error',
                                        confirmButtonText: 'Ok!'
                                    })
                                }else{
                                    Swal.fire({
                                        title: 'Exito!',
                                        text: data.mensaje,
                                        type: 'success',
                                        confirmButtonText: 'Ok!'
                                    })
                                }
                                $('#cover-spin').hide(0)
                                getDataCLiente(idCliente);
                            },
                            error: function() {
                                $('#cover-spin').hide(0)
                                Swal.fire({
                                    title: 'Error!',
                                    text: 'Usted no tiene suscripciones para cancelar',
                                    type: 'error',
                                    confirmButtonText: 'Ok!'
                                })
                            }
                            
                        }
                    );
                }
            })

        }



    </script>
</body>

<script src='https://cdn.jsdelivr.net/npm/sweetalert2'></script>
<script src="../js/select2.full.js"></script>



   
        
<script>
    var id_cliente = '<?php echo $_SESSION['cliente']['idcliente']?>'
    getDataCLiente(id_cliente);
    $('#selectCiudad').select2();

    $(document).ready(function(){
        // se crea el mapa
        map = new GMaps({
            div: '#cobertura-map',
            lat: -25.287242,
            lng: -57.634915,
            zoom: 12
        });

        positionCustomer = map.addMarker({
            lat: -25.287242,
            lng: -57.634915,
            draggable: true,
            dragend: function (event) {
                var lat = event.latLng.lat();
                var lng = event.latLng.lng();
                var latlng = new google.maps.LatLng(lat, lng);
                positionCustomer.setPosition(latlng);
                cargarInput(lat, lng);
                addressToCoordenate(lat, lng);
            },
        });


        $('#cobertura-map').css('width', 'auto');
        $('#cobertura-map').css('height', '500px');
      });

      function cargarInput(lat, lng) {
            $('#Lat').val(lat);
            $('#Lng').val(lng);
        }

        function addressToCoordenate(lat,lng){
        var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
        var address = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lng+"&key=AIzaSyDOzs_Te0C0fhCfulT7JPWee2WvNr14ws0";
        $.getJSON(address, function(response) {
            if(response.status = "OK"){
                for (var i = 0; i < response.results.length; i++) {
                    var addressType = response.results[i].types;
                    if (addressType == "route") {
                        $('#inputDireccionCalle').val(response.results[i].address_components[0]["long_name"]);
                    }
                }
            }   

        });

    }

    $('#modal-mapas').on('shown.bs.modal', function () {
        $("#modal-direccion").modal('hide');
        $('.modal .modal-body').css('overflow-y', 'auto'); 
        $('.modal .modal-body').css('max-height', $(window).height() * 0.7);
        if(!isEmpty(clienteData.cliente.direcciones_clientes)){
            map.setCenter(parseFloat(clienteData.cliente.direcciones_clientes[0].lat), parseFloat(clienteData.cliente.direcciones_clientes[0].lng));
            var latlng = new google.maps.LatLng(parseFloat(clienteData.cliente.direcciones_clientes[0].lat), parseFloat(clienteData.cliente.direcciones_clientes[0].lng));
            positionCustomer.setPosition(latlng);
            cargarInput(clienteData.cliente.direcciones_clientes[0].lat, clienteData.cliente.direcciones_clientes[0].lng);
        }

    });

    $('#modal-mapas').on('hidden.bs.modal', function () {
        $("#modal-direccion").modal('show');
    });
    
</script>


<script src="../js/gmaps.js"></script>

<style>
    .pac-container {
    z-index: 1051 !important;
}
</style>
</html>

<?php else:?>

<?php
  header("Location: ../");
?>

<?php endif;?>
