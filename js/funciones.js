
var clienteData = [];
var precioProducto = 0;
var valesData = [];
var textoVale = '';
var montoDescuento = 0;
function salir(){
    alert('alerta de click');
}

$("#formLogin").submit(function(e){
    e.preventDefault();
    var email = $('#email').val();
    var pass = $('#pass').val();

    var user = {
        "username" : email,
        "password" : pass
    };

    $.ajax({
        data:  user, //datos que se envian a traves de ajax
        url:   urlAdmin+'Users/loginAjax', //archivo que recibe la peticion
        type:  'POST', //método de envio,
        dataType:'json',
        beforeSend: function () {
            $('#cover-spin').show(0)
        },
        success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
            $.ajax ({
                data:        {"cliente": response.cliente},
                url:        urlPage+'session.php',
                type:        'post',
                success : function(){
                    $('#cover-spin').hide(0)
                    if(response.response){
                        window.location.replace(urlPage+'fotos/index.php');
                    }else{
                        $('#errorLabel').text(response.message);
                        $('#errorLabel').css('display','block');
                    }
                } 
            });
        }
    });
});

$("#formRegistro").submit(function(e){
    e.preventDefault();
    var email = $('#email_reg').val();
    var nombre = $('#nombre_reg').val();
    var pass = $('#pass_reg').val();
    var pass_two = $('#pass_two_reg').val();

    var user = {
        "pass_reg" : pass,
        "pass_two_reg" : pass_two,
        "email_reg" : email,
        "nombre_reg" : nombre
    };

    $.ajax({
        data:  user, //datos que se envian a traves de ajax
        url:   urlAdmin+'Users/addAjax', //archivo que recibe la peticion
        type:  'POST', //método de envio,
        dataType:'json',
        beforeSend: function () {
            $('#cover-spin').show(0)
        },
        success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
             if(response.response){
                 $.ajax ({
                    data:        {"cliente": response.cliente},
                    url:        urlPage+'session.php',
                    type:        'post',
                    success : function(){
                        $('#cover-spin').hide(0)
                        if(response.response){
                            window.location.replace(urlPage+'fotos/index.php');
                        }
                    } 
                });
             }else{
                $('#errorLabelReg').text(response.message);
                $('#errorLabelReg').css('display','block');
             }
            $('#cover-spin').hide(0)

        }
    });
});

function getDataCLiente(id_cliente){ 
    var cliente = {
        "id_cliente" : id_cliente
    };

    $.ajax({
        data:  cliente, //datos que se envian a traves de ajax
        url:   urlAdmin+'Clientes/getData/', //archivo que recibe la peticion
        type:  'POST', //método de envio,
        dataType:'json',
    beforeSend: function () {
        $('#cover-spin').show(0)
    },
    success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
        clienteData = response;
        if(!isEmpty(response.cliente)){
            $('#nombreText').text(response.cliente.nombre_completo)
            $('#emailText').text(response.cliente.email)
            $('#telefonoText').text(response.cliente.telefono)
            $('#documentoText').text(response.cliente.nro_documento)
        
            if(!isEmpty(response.cliente.direcciones_clientes)){
                $('#ciudadText').text(response.cliente.direcciones_clientes[0].ciudade.nombre)
                $('#barrioText').text(response.cliente.direcciones_clientes[0].barrio)
                $('#inputDireccionBarrio').val(response.cliente.direcciones_clientes[0].barrio)
                $('#calle1Text').text(response.cliente.direcciones_clientes[0].calle_principal)
                $('#inputDireccionCalle').val(response.cliente.direcciones_clientes[0].calle_principal)
                $('#nroCasaText').text(response.cliente.direcciones_clientes[0].nro_casa)
                $('#inputDireccionNroCasa').val(response.cliente.direcciones_clientes[0].nro_casa)
                $('#calle2Text').text(response.cliente.direcciones_clientes[0].calle_secundaria)
                $('#inputDireccionCalle2').val(response.cliente.direcciones_clientes[0].calle_secundaria)
                $('#referenciaText').text(response.cliente.direcciones_clientes[0].referencia)
                $('#inputDireccionReferencia').val(response.cliente.direcciones_clientes[0].referencia)
            }
        }

        if (response.vales!=null) {
            if (response.vales.activo) {
                $('#pvale').show();
                $('#valeText').text(response.vales.cantidad_disponible)
                $('#codigoValeText').text(response.vales.vale_nombre)
            }else{
                $('#pvale').hide();            
            }
        }

        if(response.suscripcion!=null){
            $('.d-sm-block h3').text(response.suscripcion.producto.nombre)
            $('.d-sm-none h3').text(response.suscripcion.producto.nombre)
            $('#planText').text(response.suscripcion.producto.nombre)
            $('#precioText').text(response.suscripcion.producto.precio)
            $('#planDiaText').text(response.suscripcion.diaDebito)
            $('#buttonCancelar').value = "Cancelar suscripción";
            $('#buttonCancelar').show();
            $('#psuscripcion').show();
            $('#psuscripcion2').show();
            
        }else{
            $('.d-sm-block h3').text('Compre un plan')
            $('.d-sm-none h3').text('Compre un plan')
            $('#buttonCancelar').value = "Agregar suscripción";
            $('#buttonCancelar').hide();
            $('#psuscripcion').hide();
            $('#psuscripcion2').hide();
        }
        
        $('#cantidad_fotos_block').text(response.cantidad_fotos)
        $('#cantidad_fotos_none').text(response.cantidad_fotos)
        $('#cantidad_fotos_seleccionadas_block').text(response.cantidad_fotos_seleccionadas)
        $('#cantidad_fotos_seleccionadas_none').text(response.cantidad_fotos_seleccionadas)

        var fotosHtml = "";
        for (let index = 0; index < response.fotos.length; index++) {
            var impreso  = response.fotos[index].impreso;
            disabled = "";
            var impresoText = ""
            if(impreso){
                disabled = "disabled";
                impresoText = "Impreso";
            }
            fotosHtml +=  '<div class="col-md-3 foto" id="div-'+response.fotos[index].idcliente_fotos+'"> ' +
                            '<img src="'+response.fotos[index].rutaImagen+'" alt="foto" id="img-'+response.fotos[index].idcliente_fotos+'" data-id="'+response.fotos[index].idcliente_fotos+'"> ' +
                            '<img class="tl" src="../img/tl.png" alt="tl">'+
                            '<div class="formulario-foto">'+
                                '<form>'+
                                    '<div class="form-group row">'+
                                        '<label class="col-md-1 col-form-label"><i class="far fa-calendar-alt"></i></label>'+
                                        '<div class="col-md-9">';
                                        var dato = ''
                                        if(response.fotos[index].dato!=null){
                                            dato = response.fotos[index].dato
                                        }
            fotosHtml +=   '<input type="text" id="fecha-'+response.fotos[index].idcliente_fotos+'" value ="'+dato+'" class="form-control-plaintext" placeholder="16/01/16">'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="form-group row">'+
                                        '<label class="col-md-1 col-form-label"><i class="far fa-comments"></i></label>'+
                                        '<div class="col-md-9">';
                                        var etiqueta = ''
                                        if(response.fotos[index].etiqueta!=null){
                                            etiqueta = response.fotos[index].etiqueta
                                        }
            fotosHtml +=   '<input type="text" id="etiqueta-'+response.fotos[index].idcliente_fotos+'" value ="'+etiqueta+'" class="form-control-plaintext" placeholder="Etiqueta">'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="form-group row">'+
                                        '<label class="col-md-1 col-form-label"><i class="far fa-heart"></i></label>'+
                                        '<div class="col-md-9">';
                                        var likes = ''
                                        if(response.fotos[index].likes!=null){
                                            likes = response.fotos[index].likes
                                        }
            fotosHtml += '<input type="text" id="like-'+response.fotos[index].idcliente_fotos+'" value ="'+likes+'" class="form-control-plaintext" placeholder="Likes">'+
                                        '</div>'+
                                    '</div>';
                                    var check = ''
                                        if(response.fotos[index].seleccionado){
                                            check = 'checked';
                                        };

            fotosHtml +=     '<div class="form-check">'+
                                        '<center>'+
                                        '<input type="checkbox" '+check+' '+disabled+' class="form-check-input" foto="'+response.fotos[index].idcliente_fotos+'" id="check-'+response.fotos[index].idcliente_fotos+'">'+
                                        '<label class="form-check-label" for="exampleCheck1">Seleccionar</label><br>'+
                                        '<label class="form-check-label" for="exampleCheck2">'+impresoText+'</label>'+
                                        '</center>'+

                                    '</div>'+
                                '</form>'+
                            '</div>'+
                        '</div>';
        

        }

        

        $('#fotos_div').html(fotosHtml);
        getCiudades();


        $(document).ready(function() {
            $('.foto img').each(function() {
    
                $(this).on('load', function(){
                    var w = $(this).width();
                    var hei = $(this).height();
                    var dataId = $(this).attr("data-id");

                    var diferencia = w - hei;
                    var diferencia2 = hei - w ;

                    
                    if (diferencia>40) {
                        $("#div-"+dataId).addClass( "rectangular" );
                    }

                    if(diferencia2>50){
                        $("#div-"+dataId).addClass( "rect-large" );
                    }
                });
                
               
            });
        });

        $('#cover-spin').hide(0);

        
    },
    error: function(response){
        $('#cover-spin').hide(0)        
    }
});}

function instagranFotos(){
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Desea actualizar sus fotos de instagram?',
        text: "No lo podrá revertir!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Sí, actualizar!',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true
      }).then((result) => {
        if (result.value) {
          window.location.replace(urlPage+"instagramgetfotos/index.php")
        }
      })
}

function facebookFotos(id_cliente){
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Desea actualizar sus fotos de facebook?',
        text: "No lo podrá revertir!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Sí, actualizar!',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true
      }).then((result) => {
        if (result.value) {
            getFotosFacebook(id_cliente);
        }
      })
}

function makeFacebookPhotoURL( id, accessToken ) {
    return 'https://graph.facebook.com/' + id + '/picture?access_token=' + accessToken;
}

function login( callback ) {
    FB.login(function(response) {
        if (response.authResponse) {
            //console.log('Welcome!  Fetching your information.... ');
            if (callback) {
                callback(response);
            }
        } else {
            console.log('User cancelled login or did not fully authorize.');
        }
    },{scope: 'user_photos'} );
}

function getAlbums( callback ) {
    FB.api(
            '/me/albums',
            {fields: 'id,cover_photo'},
            function(albumResponse) {
                //console.log( ' got albums ' );
                if (callback) {
                    callback(albumResponse);
                }
            }
        );
}

function getPhotosForAlbumId( albumId, callback ) {
    FB.api(
            '/'+albumId+'/photos',
            {fields: 'id,picture,source'},
            function(albumPhotosResponse) {
                //console.log( ' got photos for album ' + albumId );
                if (callback) {
                    callback( albumId, albumPhotosResponse );
                }
            }
        );
}

function getLikesForPhotoId( photoId, callback ) {
    FB.api(
            '/'+albumId+'/photos/'+photoId+'/likes',
            {},
            function(photoLikesResponse) {
                if (callback) {
                    callback( photoId, photoLikesResponse );
                }
            }
        );
}

function getPhotos(callback) {
    var allPhotos = [];
    var accessToken = '';
    login(function(loginResponse) {
            accessToken = loginResponse.authResponse.accessToken || '';
            getAlbums(function(albumResponse) {
                    var i, album, deferreds = {}, listOfDeferreds = [];
                    for (i = 0; i < albumResponse.data.length; i++) {
                        album = albumResponse.data[i];
                        deferreds[album.id] = $.Deferred();
                        listOfDeferreds.push( deferreds[album.id] );
                        getPhotosForAlbumId( album.id, function( albumId, albumPhotosResponse ) {
                            console.log(albumPhotosResponse);
                                var i, facebookPhoto;
                                for (i = 0; i < albumPhotosResponse.data.length; i++) {
                                    facebookPhoto = albumPhotosResponse.data[i];
                                    allPhotos.push({
                                        'id'	:	facebookPhoto.id,
                                        'added'	:	facebookPhoto.created_time,
                                        'url'	:	facebookPhoto.source
                                    });
                                }
                                deferreds[albumId].resolve();
                            });
                    }
                    $.when.apply($, listOfDeferreds ).then( function() {
                        if (callback) {
                            callback( allPhotos );
                        }
                    }, function( error ) {
                        if (callback) {
                            callback( allPhotos, error );
                        }
                    });
                });
        });
}

function getFotosFacebook(id_cliente){

    $('#cover-spin').show(0);
    var docReady = $.Deferred();
		var facebookReady = $.Deferred();
		$(document).ready(docReady.resolve);
		window.fbAsyncInit = function() {
			FB.init({
			  appId      : '972949543056619',
			  channelUrl : urlPage+'fotos/index.php',
			  status     : true,
			  cookie     : true,
			  xfbml      : true
			});
			facebookReady.resolve();
		};
		$.when(docReady, facebookReady).then(function() {
			if (typeof getPhotos !== 'undefined') {
				getPhotos( function( photos ) {

                    var p = {
                        "photos" : photos
                    };
                    $.ajax ({
                        data:        p,
                        url:         urlAdmin+'clienteFotos/insertFotosFacebook/facebook/'+id_cliente,
                        type:        'post',
                        dataType:'json',
                        beforeSend: function () {
                            $('#cover-spin').show(0)
                        },
                        success : function(){
                            window.location.replace(urlPage+'fotos/index.php');                            
                            $('#cover-spin').hide(0)
                        },
                        error : function(){
                            $('#cover-spin').hide(0)
                        }  
                     });


                });
            }
            
		});
		// call facebook script
		(function(d){
		 var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
		 js = d.createElement('script'); js.id = id; js.async = true;
		 js.src = "https://connect.facebook.net/en_US/all.js";
		 d.getElementsByTagName('head')[0].appendChild(js);
		}(document));
}

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

$('body').on('change', 'input[type="checkbox"]', function() {
    var idFoto=$(this).attr('id').replace( /\D/g, '');
    var seleccionado = true;
    if(!$(this).is(':checked'))
    {
        seleccionado=false;
    }
 $.ajax({
        url:   urlAdmin+'clienteFotos/marcarFoto/'+idFoto+'/'+seleccionado, //archivo que recibe la peticion
        type:  'POST', //método de envio,
        dataType:'json',
        beforeSend: function () {
            $('#cover-spin').show(0)
        },
        success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
            $('#cover-spin').hide(0)
            getDataCLiente(clienteData.cliente.idcliente);
        },
        error:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
            $('#cover-spin').hide(0)
        }
    });
});


$('body').on('keyup change','input[id^=fecha]',function(event) {
    var idFoto=$(this).attr('id').replace( /\D/g, '');
    var fecha = $(this).val();
    var fechaSend = {
        "fecha" : fecha
    };

 $.ajax({
        data: fechaSend,
        url:   urlAdmin+'clienteFotos/fechafoto/'+idFoto, //archivo que recibe la peticion
        type:  'POST', //método de envio,
        dataType:'json',
        success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
            $('#cover-spin').hide(0)
        },
        error:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
            $('#cover-spin').hide(0)
        }
    });
});

$('body').on('keyup change','input[id^=etiqueta]',function(event) {
    var idFoto=$(this).attr('id').replace( /\D/g, '');
    var etiqueta = $(this).val();
    var etiquetaSend = {
        "etiqueta" : etiqueta
    };

 $.ajax({
        data: etiquetaSend,
        url:   urlAdmin+'clienteFotos/etiquetafoto/'+idFoto, //archivo que recibe la peticion
        type:  'POST', //método de envio,
        dataType:'json',
        success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
            $('#cover-spin').hide(0)
        },
        error:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
            $('#cover-spin').hide(0)
        }
    });
});

$('body').on('keyup change','input[id^=like]',function(event) {
    var idFoto=$(this).attr('id').replace( /\D/g, '');
    var like = $(this).val();
    var likeSend = {
        "like" : like
    };

 $.ajax({
        data: likeSend,
        url:   urlAdmin+'clienteFotos/likefoto/'+idFoto, //archivo que recibe la peticion
        type:  'POST', //método de envio,
        dataType:'json',
        success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
            $('#cover-spin').hide(0)
        },
        error:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
            $('#cover-spin').hide(0)
        }
    });
});



function sidablebPop()
{
    Swal.fire({
        title: 'Atención!',
        text: 'Debe estar logueado para poder comprar un pack',
        type: 'info',
        confirmButtonText: 'Ok!'
    })
}

function suscripcion(clienteId, productoId){
    var textoVale = '';
    var montoDescuento = 0;
    var valeId = '';
    $.ajax({
        url:   urlAdmin+'productos/getProducto/'+productoId, //archivo que recibe la peticion
        type:  'POST', //método de envio,
        dataType:'json',
        beforeSend: function () {
            $('#cover-spin').show(0)
        },
        success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
            precioProducto = response.precio;
            $('#cover-spin').hide(0)
            if(isEmpty(clienteData.cliente.direcciones_clientes)){
                Swal.fire({
                    title: 'Atención!',
                    text: 'Debe cargar una dirección para poder comprar un pack',
                    type: 'info',
                    confirmButtonText: 'Ok!'
                })
                return;
            }

            Swal.fire({
                title: 'Cuento con un cupón de descuento?',
                input: 'text',
                inputAttributes: {
                  autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Si, confirmar',
                cancelButtonText: 'No, no tengo!',
                showLoaderOnConfirm: true,
                preConfirm: (cupon) => {
                  return fetch(urlAdmin+'vales/validarCupo/'+`${cupon}`)
                    .then(response => {
                      if (!response.ok) {
                        throw new Error(response.statusText)
                      }
                      return response.json()
                    })
                    .catch(error => {
                      Swal.showValidationMessage(
                        `Error de conexión: ${error}`
                      )
                    })
                },
                allowOutsideClick: () => !Swal.isLoading()
              }).then((result) => {

                  valesData = result.value;
                var buttonCanel = true;
                if(clienteData.cliente.direcciones_clientes[0].ciudade.solo_tarjeta){
                    buttonCanel = false;
                }
                  const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-info'
                    },
                    buttonsStyling: false
                })
          
                swalWithBootstrapButtons.fire({
                    title: 'Con qué metodo de pago va a abonar?',
                    text: "El costo puede variar de acuerdo a la ciudad en donde viva",
                    type: 'question',
                    showCancelButton: buttonCanel,
                    confirmButtonText: 'Bancard',
                    cancelButtonText: 'Efectivo',
                    reverseButtons: true
                }).then((result) => {
                    if(!isEmpty(valesData)){
                        if(valesData.response){
                            textoVale = 'Descuento: ' + formatNumber((parseInt(precioProducto)+parseInt(clienteData.cliente.direcciones_clientes[0].ciudade.precio_delivery))*parseInt(valesData.vale.porcentaje_descuento)/100)
                            montoDescuento = ((parseInt(precioProducto)+parseInt(clienteData.cliente.direcciones_clientes[0].ciudade.precio_delivery))*parseInt(valesData.vale.porcentaje_descuento)/100);
                            valeId = valesData.vale.idvale
                        }else{
                            textoVale = 'Descuento: ' + valesData.mensaje;
                        }
                    }
                    if (result.value) {
                        const swalWithBootstrapButtons = Swal.mixin({
                            customClass: {
                            confirmButton: 'btn btn-success',
                            cancelButton: 'btn btn-danger'
                            },
                            buttonsStyling: false
                        })
                        
                        Swal.fire({
                            title: 'Esta seguro de susribirse a este paquete?',
                            html:  'Precio: ' + formatNumber(precioProducto) + '<br>Costo delivery: ' +formatNumber(clienteData.cliente.direcciones_clientes[0].ciudade.precio_delivery) + '<br>Dias de entrega: ' +clienteData.cliente.direcciones_clientes[0].ciudade.dias_entrega  + '<br>' + textoVale + '<br>Total: ' + formatNumber(parseInt(precioProducto)+parseInt(clienteData.cliente.direcciones_clientes[0].ciudade.precio_delivery)-montoDescuento),
                            type: 'question',
                            showCancelButton: true,
                            confirmButtonText: 'Si, suscribirme',
                            cancelButtonText: 'No, cancelar',
                            reverseButtons: true,
                            showLoaderOnConfirm: true,
                            preConfirm: (vale) => {
                                console.log(vale)
                            },
                            allowOutsideClick: () => !Swal.isLoading()
                        }).then((result) => {
                            if (result.value) {
                                $.ajax({
                                    url:   urlAdmin+'suscripciones/suscripcionBancard/'+clienteId+'/'+productoId+'/Bancard/'+'/'+valeId, //archivo que recibe la peticion
                                    type:  'POST', //método de envio,
                                    dataType:'json',
                                    beforeSend: function () {
                                        $('#cover-spin').show(0)
                                    },
                                    success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                                        if(response.tipo=="cuenta"){
                                            swalWithBootstrapButtons.fire({
                                                title: response.mensaje,
                                                text: "Por favor agreguelo a su cuenta!!!",
                                                type: 'info',
                                                showCancelButton: true,
                                                confirmButtonText: 'Ir a mi cuenta!',
                                                cancelButtonText: 'No ir!',
                                                reverseButtons: true
                                            }).then((result) => {
                                                window.location.replace(urlPage+'cuenta/index.php');
                                            });
                                            $('#cover-spin').hide(0)
                                
                                            return;
                                        }
                                        if(response.response==true){
                                            Swal.fire({
                                                title: 'Exito!',
                                                text: response.mensaje,
                                                type: 'success',
                                                confirmButtonText: 'Ok!'
                                            })
                                        }else{
                                            Swal.fire({
                                                title: 'Error!',
                                                text: response.mensaje,
                                                type: 'error',
                                                confirmButtonText: 'Ok!'
                                            })
                                        }
                                        $('#cover-spin').hide(0)
                                    },
                                    error: function(){
                                        $('#cover-spin').hide(0)
                                    }
                                });
                            }
                        })
                    }else{
                        Swal.fire({
                            title: 'Esta seguro de susribirse a este paquete?',
                            html:  'Precio: ' + formatNumber(precioProducto) + '<br>Costo delivery: ' +formatNumber(clienteData.cliente.direcciones_clientes[0].ciudade.precio_delivery) + '<br>Dias de entrega: ' +clienteData.cliente.direcciones_clientes[0].ciudade.dias_entrega  + '<br>' + textoVale + '<br>Total: ' + formatNumber(parseInt(precioProducto)+parseInt(clienteData.cliente.direcciones_clientes[0].ciudade.precio_delivery)-montoDescuento),
                            type: 'question',
                            showCancelButton: true,
                            confirmButtonText: 'Si, suscribirme',
                            cancelButtonText: 'No, cancelar',
                            reverseButtons: true,
                            showLoaderOnConfirm: true,
                            preConfirm: (vale) => {
                                console.log(vale)
                            },
                            allowOutsideClick: () => !Swal.isLoading()
                        }).then((result) => {
                            if (result.value) {
                                $.ajax({
                                    url:   urlAdmin+'suscripciones/suscripcionEfectivo/'+clienteId+'/'+productoId+'/Efectivo'+'/'+valeId, //archivo que recibe la peticion
                                    type:  'POST', //método de envio,
                                    dataType:'json',
                                    beforeSend: function () {
                                        $('#cover-spin').show(0)
                                    },
                                    success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                                        if(response.response==true){
                                            Swal.fire({
                                                title: 'Exito!',
                                                text: response.mensaje,
                                                type: 'success',
                                                confirmButtonText: 'Ok!'
                                            })
                                        }else{
                                            Swal.fire({
                                                title: 'Error!',
                                                text: response.mensaje,
                                                type: 'error',
                                                confirmButtonText: 'Ok!'
                                            })
                                        }
                                        $('#cover-spin').hide(0)
                                    },
                                    error: function(){
                                        $('#cover-spin').hide(0)
                                    }
                                });
                            }
                        })

                    }
                })


              })
              
           
        },
        error: function(){
            $('#cover-spin').hide(0)
        }
    });
}

function formatNumber(num){
    if (!num || num == 'NaN') return '-';
    if (num == 'Infinity') return '&#x221e;';
    num = num.toString().replace(/\$|\,/g, '');
    if (isNaN(num))
        num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num * 100 + 0.50000000001);
    num = Math.floor(num / 100).toString();
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3) ; i++)
        num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));
    return (((sign) ? '' : '-') + num );
}



function isset ( strVariableName ) { 
    try { 
        eval( strVariableName );
    } catch( err ) { 
        if ( err instanceof ReferenceError ) 
            return false;
    }
    return true;
}

function editUser(clienteId) {
    var user = {
        "Clientes":{
            "nombre_completo": $('#inputCuentaNombre').val(),
            "email": $('#inputCuentaEmail').val(),
            "telefono": $('#inputCuentaTelefono').val(),
            "nro_documento": $('#inputCuentaDocumento').val()
        } 
    };

    $.ajax({
        data:  user, //datos que se envian a traves de ajax
        url:   urlAdmin+'clientes/editAjax/'+clienteId, //archivo que recibe la peticion
        type:  'POST', //método de envio,
        dataType:'json',
        beforeSend: function () {
            $('#cover-spin').show(0)
        },
        success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
            $('#cover-spin').hide(0)
            $.ajax ({
                data:        {"cliente": response.cliente},
                url:        urlPage+'session.php',
                type:        'post',
                success : function(){
                    if(response.response){
                        Swal.fire({
                            title: 'Exito!',
                            text: response.mensaje,
                            type: 'success',
                            confirmButtonText: 'Ok!'
                        })
                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: response.mensaje,
                            type: 'error',
                            confirmButtonText: 'Ok!'
                        })
                    }
                    getDataCLiente(response.cliente.idcliente);
                } 
            });

           
        }
    });
}


function editAddress(clienteId="") {
    var cliente = {
        "Clientes":{
            "nombre_completo": $('#inputCuentaNombre').val(),
            "email": $('#inputCuentaEmail').val(),
            "telefono": $('#inputCuentaTelefono').val(),
            "nro_documento": $('#inputCuentaDocumento').val()
        } 
    };

    $.ajax({
        data:  cliente, //datos que se envian a traves de ajax
        url:   urlAdmin+'clientes/editAjax/'+clienteId, //archivo que recibe la peticion
        type:  'POST', //método de envio,
        dataType:'json',
        beforeSend: function () {
            $('#cover-spin').show(0)
        },
        success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
            $('#cover-spin').hide(0)
            $.ajax ({
                data:        {"cliente": response.cliente},
                url:        urlPage+'session.php',
                type:        'post',
                success : function(){
                    if(response.response){
                        Swal.fire({
                            title: 'Exito!',
                            text: response.mensaje,
                            type: 'success',
                            confirmButtonText: 'Ok!'
                        })
                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: response.mensaje,
                            type: 'error',
                            confirmButtonText: 'Ok!'
                        })
                    }
                    getDataCLiente(response.cliente.idcliente);
                } 
            });

           
        }
    });
}


function getCiudades(){
    $.ajax({
        url:   urlAdmin+'ciudades/getCiudades/', //archivo que recibe la peticion
        type:  'POST', //método de envio,
        dataType:'json',
        beforeSend: function () {
            $('#cover-spin').show(0)
        },
        success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
            var options = [];
            options.push('<option value="">',
                  "Seleccione una Ciudad" , '</option>');
            for (var i = 0; i < response.length; i++) {
                options.push('<option value="',
                  response[i].idciudades, '">',
                  response[i].nombre + ' Delivery: Gs. ' + formatNumber(response[i].precio_delivery) , '</option>');
            }
            $("#selectCiudad").html(options.join(''));

            if(!isEmpty(clienteData.cliente)){
                if(!isEmpty(clienteData.cliente.direcciones_clientes)){
                $("#selectCiudad").val(clienteData.cliente.direcciones_clientes[0].ciudade.idciudades);
                }
            }
            $('#cover-spin').hide(0);
        }
    });
}


function addEditAddress() {

    if(!isEmpty(clienteData.cliente.direcciones_clientes)){
        var direccion = {
            "DireccionesClientes":{
                "ciudad_id": $('#selectCiudad').val(),
                "barrio": $('#inputDireccionBarrio').val(),
                "calle_principal": $('#inputDireccionCalle').val(),
                "nro_casa": $('#inputDireccionNroCasa').val(),
                "calle_secundaria": $('#inputDireccionCalle2').val(),
                "referencia": $('#inputDireccionReferencia').val(),
                "lat": $('#Lat').val(),
                "lng": $('#Lng').val(),
                "iddireccion": clienteData.cliente.direcciones_clientes[0].iddireccion
            } 
        };
    }else{
        var direccion = {
            "DireccionesClientes":{
                "ciudad_id": $('#selectCiudad').val(),
                "barrio": $('#inputDireccionBarrio').val(),
                "calle_principal": $('#inputDireccionCalle').val(),
                "nro_casa": $('#inputDireccionNroCasa').val(),
                "calle_secundaria": $('#inputDireccionCalle2').val(),
                "referencia": $('#inputDireccionReferencia').val(),
                "lat": $('#Lat').val(),
                "lng": $('#Lng').val(),
                "cliente_id": clienteData.cliente.idcliente
            } 
        };
    }

   
    $.ajax({
        data:direccion,
        url:   urlAdmin+'direccionesClientes/addEditAddress/', //archivo que recibe la peticion
        type:  'POST', //método de envio,
        dataType:'json',
        beforeSend: function () {
            //$('#cover-spin').show(0)
        },
        success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
            $('#cover-spin').hide(0);
            if(response.response){
                Swal.fire({
                    title: 'Exito!',
                    text: response.mensaje,
                    type: 'success',
                    confirmButtonText: 'Ok!'
                })
            }else{
                Swal.fire({
                    title: 'Error!',
                    text: response.mensaje,
                    type: 'error',
                    confirmButtonText: 'Ok!'
                })
            }
            getDataCLiente(clienteData.cliente.idcliente);
        }
    });
}


function subitFotos() {
    $("#modal-fotos").modal();
}

function abrirMapa() {
    $("#modal-mapas").modal();
}
